/*file name: Bondarenko_Olexandr1_3_lab3_1.cpp
*�������: ���������� ��������� ³��������
*�����: ��-1-3
*���� ���������: 10/11/2021
*���� �������� ����: 10/11/2021
*����������� ������ �3
*����: "������������� �� ������������� ����� ������� ��������� p ������������� ����� � ���������� FOR"
*��������: ��������� ����-����� ��������� ����������� ������� y �� �������� ������� [xstart�xend] �� ���������� ���� ����� �\�++ �������� ������� �������������� ��������,
�� �������� � ������� E.1 ������� E. ����������� �������� ������� �������, �� ����� ���� �������� ����������� �������, � ����� ��������� xstep ���� ���������.
*/

#include <iostream>
#include <math.h>
#include <stdio.h>
#include <windows.h>

using namespace std;
int main()
{
setlocale(LC_ALL , "Russian");
system("cls");
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
double x, y, xstart, xend, xstep;
cout<<" ������ �������� xstart..xend \n";
cout<<" ������ xstart = ";
cin>>xstart;
cout<<" ������ xend = ";
cin>>xend;
cout<<" ������ xstep = ";
cin>>xstep;
printf("_______________________\n");
printf("|     x    |     y    |\n");
printf("-----------------------\n");
for (x=xstart;x<=xend;x=x+xstep)
{
if (x<=0)
y=0.5*x+(pow(tan(x),2));
else 
y=log10(x)+0.7;
printf("| %8.4f | %8.4f |\n", x,y); 
printf("-----------------------\n");	
}
system("pause");
return 0;
}
