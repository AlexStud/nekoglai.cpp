#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("cls");
	srand(time(NULL));
	const int n=5,m=5;
	int mas[n][m],i,j,pi,pj,d=0,k=0,v=0,tmp ;
	cout << "mas[5][5]:" << endl;
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			mas[i][j]=rand()%20-10;
			if(mas[i][j]>0)
				v++;
			printf("%3d",mas[i][j]);
		}
		cout << endl;
	}
	int a[v];
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			if(mas[i][j]>0)
			{
				a[k]=mas[i][j];
				k++;
			}
		}
	}
	for(k=v/2;k>0;k/=2)
	{
		for(i=k;i<v;i++)
		{
			tmp=a[i];
			for(j=i;j>=k;j-=k)
			{
				if(tmp<a[j-k])
					a[j]=a[j-k];
				else
					break;
			}
			a[j]=tmp;
		}
	}
	for(k=0;k<n+m-1;k++)
	{
		if(k<n)
		{
			pi=n-1-k;
			pj=0;
		}
		else
		{
			pi=0;
			pj=k-m+1;
		}
		for(;pi<n&pj<m;pi++,pj++)
		{
			if(mas[pi][pj]>0)
			{
				mas[pi][pj]=a[d];
				d++;
			}
		}
	}
	cout << endl <<  "Отсортированая матрица: " << endl;
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			printf("%3d",mas[i][j]);
		}
		cout << endl;
	}
	system("Pause");
	return 0;
}

