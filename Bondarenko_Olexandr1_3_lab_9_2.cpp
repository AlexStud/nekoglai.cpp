#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("cls");
	srand(time(NULL));
	const int n=5,m=5,v=n+m;
	int i,j,mas[n][m],a[v],k=0;
	cout << "mas[5][5]:" << endl;
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			mas[i][j]=rand()%20-10;
			printf("%3d",mas[i][j]);
		}
		cout << endl;
	}
	for(j=0;j<m;j++)
	{
		for(i=j+1;i<n;i++)
		{
			a[k]=mas[i][j];
			k++;
		}
	}
	for(i=1;i<v;i++)
	{
		for(j=0;j<v-i;j++)
		{
			if(a[j]<a[j+1])
				swap(a[j],a[j+1]);
		}
	}
	k=0;
	for(j=0;j<m;j++)
	{
		for(i=j+1;i<n;i++)
		{
			mas[i][j]=a[k];
			k++;
		}
	}
	cout << "Отсортированая матрица: " << endl;
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			printf("%3d",mas[i][j]);
		}
		cout << endl;
	}
	system("Pause");
	return 0;
}

