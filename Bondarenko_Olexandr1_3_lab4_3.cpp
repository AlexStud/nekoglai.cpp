/*file name: Bondarenko_Olexandr1_3_lab4_3.cpp
*�������: ���������� ��������� ³��������
*�����: ��-1-3
*���� ���������: 24/11/2021
*���� �������� ����: 24/11/2021
*����������� ������ �4
*����: "������������� �� ������������� ����� ������� ��������� � ������������� ����� � ����������� WHILE"
*��������: ��������� ����-����� ��������� ��� ����������� ������� �� ���������� ���� ����� �\�++ �������� ������� �������������� ��������, �� �������� � ������� �.3 ������� �.
����������� �������� ������� ��������� �������,�� ����� ���� �������� ����������� �������, � ����� ����� ���� ���������.
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main() {
  system("cls");
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  double x, y, xstart, xend, xstep;
  cout<<" ������ xstart = ";
  cin>>xstart;
  cout<<" ������ xend = ";
  cin>>xend;
  cout<<" ������ xstep = ";
  cin>>xstep;
  printf("_______________________\n");
  printf("|    x     |     y    |\n");
  printf("-----------------------\n");
  x=xstart-xstep;
  while (x<=xend)
    {
    x=x+xstep;
      y=pow(x,3)-1.175*x+0.75;
    printf("| %8.4f | %8.4f |\n", x,y);
    printf("-----------------------\n");
    }

  system("pause");
  return 0;
}
