/*file name: Bondarenko_Olexandr1_3_lab3_2.cpp
*�������: ���������� ��������� ³��������
*�����: ��-1-3
*���� ���������: 10/11/2021
*���� �������� ����: 10/11/2021
*����������� ������ �3
*����: "������������� �� ������������� ����� ������� ��������� p ������������� ����� � ���������� FOR"
*��������:��������� ����-����� ��������� ��� ��������� ��� �� �������, �������� �������������� ��������, �� �������� � ������� E.2 ������� E, �� ���������� ���� ����� �\�++.
�������� ��������� ��� ��������� ��� �� ������� �������� � ���������.
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
  HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));  
}
int main() {
  system("cls");
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  double sum,multi,fact, n, n_start, n_end;
  cout<<" ������ �������� �������� n = ";
  cin>>n_start;
  cout<<" ������ ������ �������� n = ";
  cin>>n_end;
  for(n=n_start; n<=n_end;n++){
      multi=1;
      for(int i=1;i<=n+1;i++){
        multi=multi*i;
      }
      sum=sum+(n+1)/multi;
  }
  
  cout<<" Sum= "<<sum<<endl;
  system("pause");
  return 0;
}
